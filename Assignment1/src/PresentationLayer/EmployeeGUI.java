package PresentationLayer;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import DataAccessLayer.BankAccount;
import DataAccessLayer.Customer;
import DataAccessLayer.CustomerDAO;
import DataAccessLayer.Employee;
import DataAccessLayer.EmployeeDAO;

import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;


public class EmployeeGUI {

	
	private JFrame frame;
	private JButton btnCreate;
	private JButton btnUpdate;
	private JButton btnDelete;
	private JButton seeAcc;
	private JButton btnProcBill;
	private JLabel  lblWithdraw;
	
	private JRadioButton radio1, radio2;
	private boolean oneAccountOP= true;
	private JComboBox<BankAccount> comboAcc1;
	private JComboBox<BankAccount> comboAcc2;
	
	private Object[][] databaseResults;
	private Object[] columns = { "Name", "Identity Card Nb", "Personal Num. Code", "Address", "Phone Nb." };

	private DefaultTableModel dTableModel = new DefaultTableModel(
								databaseResults, columns);
	
	private void groupButton(){
		ButtonGroup bg1 = new ButtonGroup();
		bg1.add(radio1);
		bg1.add(radio2);
		}
	
	private JTable table = new JTable(dTableModel)
	{
		private static final long serialVersionUID = 1L;
		public boolean isCellEditable(int row, int col)
		{
		return false;
		}
	};
	private JLabel lblAccountOperations;
	private JButton button;
	private JButton button_1;
	private JTextField textFieldSum;
	
	private void  drawMyTable()
	{
			int[] columnsWidth = { 130, 130, 130, 130, 130};
			int ii = 0;
			for (int width : columnsWidth) {
				TableColumn column = table.getColumnModel().getColumn(ii++);
				column.setMinWidth(width);
				column.setMaxWidth(width);
				column.setPreferredWidth(width);
			}

			try {
				Object[] row;
				CustomerDAO cDAO = new CustomerDAO();
				ResultSet result = cDAO.queryForTable();
				while (result.next()) {
					row = new Object[] { result.getString(2), result.getString(3), result.getString(4), 
										 result.getString(5), result.getString(6)};
					dTableModel.addRow(row);
				}
			} catch (Exception ex) {
			}
	}
	private void updateTable()
	{
		int c = dTableModel.getRowCount();
			for (int i=c-1; i>=0; i--)
			{
				dTableModel.removeRow(i);
			}
			drawMyTable();
	}
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EmployeeGUI window = new EmployeeGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EmployeeGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
			
		
		
		frame = new JFrame("Manager Window");
		frame.setBounds(100, 100, 884, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		btnCreate = new JButton("Add Cusomer");
		btnCreate.setBounds(27, 12, 166, 29);
		panel.add(btnCreate);
		
		btnUpdate = new JButton("Update Customer");
		btnUpdate.setBounds(27, 108, 166, 29);
		panel.add(btnUpdate);
		
		btnDelete = new JButton("Delete Customer");
		btnDelete.setBounds(27, 148, 166, 29);
		panel.add(btnDelete);
		
		table.setFont(new Font("Serif", Font.PLAIN, 18));
		table.setRowHeight(table.getRowHeight() + 5);
		table.setAutoCreateRowSorter(true);
		table.setColumnSelectionAllowed(false);
		table.setRowSelectionAllowed(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(203, 11, 663, 279);
		panel.add(scrollPane);
		
		JLabel lblNewLabel = new JLabel("<html>The next two buttons<br>work with selected row.<br>Leaving any field empty<br>will have no effect!</html>");
		lblNewLabel.setBounds(37, 34, 141, 80);
		panel.add(lblNewLabel);
		
		lblAccountOperations = new JLabel("Account Operations:");
		lblAccountOperations.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblAccountOperations.setBounds(22, 275, 185, 29);
		panel.add(lblAccountOperations);
		
		radio1 = new JRadioButton("1 Account Operation");
		radio1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		radio1.setBounds(230, 314, 172, 23);
		panel.add(radio1);
		
		radio2 = new JRadioButton("2 Accounts Operation");
		radio2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		radio2.setBounds(565, 314, 172, 23);
		panel.add(radio2);
		
		seeAcc = new JButton("See Accounts");
		seeAcc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		seeAcc.setBounds(27, 308, 166, 29);
		panel.add(seeAcc);
		
		comboAcc1 = new JComboBox();
		comboAcc1.setBounds(230, 393, 231, 22);
		panel.add(comboAcc1);
		
		comboAcc2 = new JComboBox();
		comboAcc2.setBounds(570, 393, 231, 22);
		panel.add(comboAcc2);
		
		btnProcBill = new JButton("Process Bill!");
		btnProcBill.setBounds(5, 430, 188, 29);
		panel.add(btnProcBill);
		btnProcBill.setVisible(false);
		
		textFieldSum = new JTextField();
		textFieldSum.setBounds(5, 390, 188, 29);
		panel.add(textFieldSum);
		textFieldSum.setColumns(10);
		textFieldSum.setVisible(false);
		
		lblWithdraw = new JLabel();
		lblWithdraw.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblWithdraw.setBounds(10, 362, 183, 14);
		panel.add(lblWithdraw);
		lblWithdraw.setVisible(false);
		comboAcc2.setVisible(false);
		
		groupButton();
		
		drawMyTable();
		
	radio1.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			if (radio1.isSelected())
			{
				oneAccountOP=true;
				System.out.println(oneAccountOP);
				comboAcc2.setVisible(false);
				textFieldSum.setVisible(true);
				lblWithdraw.setVisible(true);
				btnProcBill.setVisible(true);
				
				
				lblWithdraw.setText("Select sum to widthdraw");
				btnProcBill.setText("Widthdraw Sum!");
			}
		}
	});	
		
	radio2.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			if (radio2.isSelected())
			{
				oneAccountOP=false;
				System.out.println(oneAccountOP);
				comboAcc2.setVisible(true);
				//lblWithdraw.setVisible(false);
				
				//textFieldSum.setVisible(false);
				//lblWithdraw.setVisible(false);
				lblWithdraw.setText("Select sum to transfer");
				//btnProcBill.setVisible(false);
				btnProcBill.setText("Transfer Sum!");
			}
		}
	});
	

		
btnCreate.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent e) {
		   JTextField custName = new JTextField();
		   JTextField custIDCardNb= new JTextField();
		   JTextField custPersNumCode= new JTextField();
		   JTextField custAddress= new JTextField();
		   JTextField custPhoneNb= new JTextField();

		   
		   Object[] message = { "Name: ",custName, "ID Card Number: ",custIDCardNb,
				   				"Personal Numeric Code: ",custPersNumCode,"Address: ", 
				   				custAddress, "Phone Number",custPhoneNb};

		   Object[] options = { "Create!", "Cancel" };
		   int n = JOptionPane
				   .showOptionDialog(new JFrame(), message,
						   "Add Employee User", JOptionPane.YES_NO_OPTION,
						   JOptionPane.QUESTION_MESSAGE, null, options,options[1]);
		if (n == JOptionPane.OK_OPTION) { // Afirmative
			
			   String newCustName=custName.getText();
			   String newCustIDCardNb=custIDCardNb.getText();
			   String newCustPersNumCode=custPersNumCode.getText();
			   String newCustAddress=custAddress.getText();
			   String newCustPhoneNb=custPhoneNb.getText();
						
			CustomerDAO addToDb = new CustomerDAO();
						
			Customer newCustomer = new Customer(newCustName, newCustIDCardNb, newCustPersNumCode, 
													newCustAddress, newCustPhoneNb);
			
			newCustomer.toString();
			
			if (newCustName.trim().length() > 0
					&&
					newCustIDCardNb.trim().length() > 0
					&&
					newCustPersNumCode.trim().length() > 0
					&&
					newCustAddress.trim().length() > 0
					&&
					newCustPhoneNb.trim().length() > 0	
				)//in case ALL fields are completed
			{
				System.out.println(newCustomer.toString());
				if(addToDb.checkPreValidity(newCustIDCardNb, newCustPersNumCode))//check if username is not taken
				{
					addToDb.addCustomer(newCustomer);
					updateTable();
					JOptionPane.showMessageDialog(frame,"Sucessfully added desk employee " +newCustName+"!","Add Desk Employee!",
							JOptionPane.INFORMATION_MESSAGE);
				}
				else 
				{
					JOptionPane.showMessageDialog(frame,
						    "Sorry, person "+newCustName+" is already registered!",
						    "USERNAME TAKEN",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		if (n == JOptionPane.CLOSED_OPTION) { // closed the dialog
			// ....
		}

	}
});


btnDelete.addActionListener(new ActionListener() {
	
	
	
	
	public void actionPerformed(ActionEvent e) {
		if (table.getRowCount()>0)//checking if table is empty
		{
			if(table.getSelectedRow()!=-1)//check if there is a row selected for deletion
			{
				int n = JOptionPane.showConfirmDialog(
					    frame,
					    "Are you sure you want to remove customer: "+(String)table.getValueAt(table.getSelectedRow(),0),
					    "Please confirm deletion!",
					    JOptionPane.YES_NO_OPTION);	
				if (n==JOptionPane.YES_OPTION)
				{
						CustomerDAO cDAO = new CustomerDAO();
						
						String nameForDeletion = (String)table.getValueAt(table.getSelectedRow(),0);
						String a = (String)table.getValueAt(table.getSelectedRow(),1);
						String b = (String)table.getValueAt(table.getSelectedRow(),2);

						cDAO.removeCustomer(a,b);
						updateTable();
						JOptionPane.showMessageDialog(frame,"Sucessfully Deleted customer: "+nameForDeletion+"!","Remove customer!",
								JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}
	}
});

btnUpdate.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		if (table.getRowCount()>0)//checking if table is empty
		{
			if (table.getSelectedRow()!=-1)//check if row for update is selected
			{
				String a = (String)table.getValueAt(table.getSelectedRow(),0);//username from selected row
				String b = (String)table.getValueAt(table.getSelectedRow(),1);//password from selected row
				String c = (String)table.getValueAt(table.getSelectedRow(),2);
				String d = (String)table.getValueAt(table.getSelectedRow(),3);
				String ee = (String)table.getValueAt(table.getSelectedRow(),4);
				
				
				   JTextField custName = new JTextField(a);
				   JTextField custIDCardNb= new JTextField(b);
				   JTextField custPersNumCode= new JTextField(c);
				   JTextField custAddress= new JTextField(d);
				   JTextField custPhoneNb= new JTextField(ee);
				   
				 Object[] message = { "Name: ",custName, "ID Card Number: ",custIDCardNb,
		   				"Personal Numeric Code: ",custPersNumCode,"Address: ", 
		   				custAddress, "Phone Number",custPhoneNb};

				Object[] options = { "Update!", "Cancel" };
				int n = JOptionPane.showOptionDialog(new JFrame(), message,"Update Customer INFO", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE, null, options,options[1]);
				if (n == JOptionPane.OK_OPTION) { // Afirmative
					CustomerDAO cDAO = new CustomerDAO();
			
					String newCustName = custName.getText();
					String newCustIDCardNb = custIDCardNb.getText();
					String newCustPersNumCode = custPersNumCode.getText();
					String newCustAddress = custAddress.getText();
					String newCustPhoneNb = custPhoneNb.getText();
					
					//check for empty fields
					if (newCustName.trim().length() > 0
							&&
							newCustIDCardNb.trim().length() > 0
							&&
							newCustPersNumCode.trim().length() > 0
							&&
							newCustAddress.trim().length() > 0
							&&
							newCustPhoneNb.trim().length() > 0	
						)//in case ALL fields are completed
						{
							if (cDAO.checkPreValidity(newCustIDCardNb, newCustPersNumCode)){
							cDAO.updateCustomer(b, c, 
									newCustName, newCustIDCardNb, newCustPersNumCode, newCustAddress, newCustPhoneNb);
							updateTable();
							JOptionPane.showMessageDialog(frame,"Sucessfully updated client Info"+newCustName+"!", 
									"Update Client INFO!",JOptionPane.INFORMATION_MESSAGE);
							}else{//if we already have this username
								//if prevalidity has failed, but selection we want to keep username and change password
								if (c.equals(newCustPersNumCode)&&b.equals(newCustIDCardNb))//if selected row coincides with inputed username
								{ //than we clearly wish to update the selected user's account
									cDAO.updateCustomer(b, c,
									 newCustName ,
									 newCustIDCardNb ,
									 newCustPersNumCode ,
									 newCustAddress ,
									 newCustPhoneNb);
									updateTable();
									JOptionPane.showMessageDialog(frame,"Sucessfully updated client Info "+newCustName+"!", "Update Client Info!",
												JOptionPane.INFORMATION_MESSAGE);
								}
								else{
									JOptionPane.showMessageDialog(frame,
										    "Sorry, can't update to a taken Personal Numerical Code or ID Number!",
										    "PERSON ALREADY EXISTS IN DATABASE!",
										    JOptionPane.ERROR_MESSAGE);
								}
							}	
						}
					}
						
				}
			
				}
		}
});


		
	}
}










