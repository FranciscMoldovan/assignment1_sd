package PresentationLayer;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.plaf.basic.BasicBorders.RadioButtonBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.JSlider;

import DataAccessLayer.Employee;
import DataAccessLayer.EmployeeDAO;

public class UI {

	private JFrame frame;
	private JRadioButton radioLogIn;
	private JRadioButton radioRegister;
	private JButton btn;
	private JTextField textField;
	private JPasswordField passwordField;
	private JLabel lbl1;
	private JLabel lbl2;
	private JRadioButton radioAdmin;
	private JRadioButton radioFront;
	private ButtonGroup bg1;
	private ButtonGroup bg2;
	private JLabel lblType;
	
	private String newUsername;
	private String newPassword;
	private int newRole=5; 
	
	
	private void groupButton(){
		ButtonGroup bg1 = new ButtonGroup();
		bg1.add(radioLogIn);
		bg1.add(radioRegister);
		
		ButtonGroup bg2 = new ButtonGroup();
		bg2.add(radioAdmin); 
		bg2.add(radioFront);
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UI window = new UI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("LogIn or Register");
		frame.setBounds(100, 100, 478, 378);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		radioLogIn = new JRadioButton("LogIn");
		radioLogIn.setBounds(57, 154, 109, 23);
		frame.getContentPane().add(radioLogIn);
		
		radioRegister = new JRadioButton("Register");
		radioRegister.setBounds(224, 154, 109, 23);
		frame.getContentPane().add(radioRegister);
		
		btn = new JButton("select action");
		btn.setBounds(35, 221, 137, 38);
		frame.getContentPane().add(btn);
		
		textField = new JTextField();
		textField.setBounds(197, 36, 203, 38);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(197, 109, 203, 38);
		frame.getContentPane().add(passwordField);
		
		lbl1 = new JLabel("select action");
		lbl1.setBounds(35, 48, 152, 14);
		frame.getContentPane().add(lbl1);
		
		lbl2 = new JLabel("select action");
		lbl2.setBounds(35, 121, 152, 14);
		frame.getContentPane().add(lbl2);
		
		radioFront = new JRadioButton("Front Desk ");
		radioFront.setBounds(291, 259, 109, 23);
		frame.getContentPane().add(radioFront);
		radioFront.setVisible(false);
		
		radioAdmin = new JRadioButton("Administrator");
		radioAdmin.setBounds(291, 285, 109, 23);
		frame.getContentPane().add(radioAdmin);
		
		lblType = new JLabel("Select Account Type");
		lblType.setBounds(273, 232, 169, 14);
		frame.getContentPane().add(lblType);
		radioAdmin.setVisible(false);
		lblType.setVisible(false);
		
		groupButton();
		
	    radioLogIn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (radioLogIn.isSelected())
					btn.setText("LOGIN");
					lbl1.setText("LOGIN USER");
					lbl2.setText("LOGIN PASSWORD");
					
					radioFront.setVisible(false);
					radioAdmin.setVisible(false);
					lblType.setVisible(false);
			}
		});
	    
	    radioRegister.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (radioRegister.isSelected())
				{
					btn.setText("REGISTER");
					lbl1.setText("REGISTER USER");
					lbl2.setText("REGISTER PASSWORD");
					radioFront.setVisible(true);
					radioAdmin.setVisible(true);
					lblType.setVisible(true);
				}
				
			}
		});
	    
	    
	    
		radioFront.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (radioFront.isSelected())
				{
					newRole = 0;
				}
			}
		});
	    
		
		radioAdmin.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (radioAdmin.isSelected())
				{
					newRole = 1;
				}
			}
		});
		
	    		btn.addActionListener(new ActionListener() {			
					@Override
					public void actionPerformed(ActionEvent e) {
	                   if ( btn.getText() == "REGISTER")	
	                   {
	                	   newUsername=textField.getText();
	                	   newPassword=String.valueOf(passwordField.getPassword());
	                	   EmployeeDAO addToDb = new EmployeeDAO(); 
	                	   Employee newEmployee = new Employee(newUsername, newPassword, newRole);
	                	   System.out.println(newEmployee.toString());
	                	   addToDb.addEmployee(newEmployee);
	                	   
	                	   if (newRole==0)
	                	   JOptionPane.showMessageDialog(frame,"Sucessfully added desk employee "+newUsername+"!", 
	                			   "Add Desk Employee!", JOptionPane.INFORMATION_MESSAGE);
	                	   else if (newRole==1)
	                	   JOptionPane.showMessageDialog(frame,"Sucessfully added manager "+newUsername+"!", 
	                			   "Add Manager!", JOptionPane.INFORMATION_MESSAGE);
	                		   
	                	   
	                   }
	                   if (btn.getText()=="LOGIN")
	                   {
	                	   EmployeeDAO logInToDb = new EmployeeDAO();
	                	   if(logInToDb.logInEmployee(textField.getText(), String.valueOf(passwordField.getPassword())))
	                	   {
	                		   JOptionPane.showMessageDialog(frame,"LOGIN SUCCESSFUL!","LOGIN OK", 
	                				   JOptionPane.INFORMATION_MESSAGE);
	                	   }
	                	   else 
	                	   {
	                		   JOptionPane.showMessageDialog(frame,"LogIn unsuccessful!","LOGIN ERROR",
	                				    JOptionPane.ERROR_MESSAGE);
	                	   }
	                   }
					}
				});
		
	
	}
}













