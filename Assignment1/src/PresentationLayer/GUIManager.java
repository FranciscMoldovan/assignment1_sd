package PresentationLayer;

import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import DataAccessLayer.Employee;
import DataAccessLayer.EmployeeDAO;
import javax.swing.JLabel;

public class GUIManager {

	private JFrame frame;
	private JButton btnCreate;
	private JButton btnUpdate;
	private JButton btnDelete;
	
	private Object[][] databaseResults;
	private Object[] columns = { "userEmployee", "passwordEmployee" };

	private DefaultTableModel dTableModel = new DefaultTableModel(
								databaseResults, columns);
	
	private JTable table = new JTable(dTableModel)
	{
		private static final long serialVersionUID = 1L;
		public boolean isCellEditable(int row, int col)
		{
		return false;
		}
	};
	private JLabel lblTheTwoButtons;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIManager window = new GUIManager();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUIManager() {
		initialize();
	}

	private void drawMyTable()
	{
			int[] columnsWidth = { 220, 220 };
			int ii = 0;
			for (int width : columnsWidth) {
				TableColumn column = table.getColumnModel().getColumn(ii++);
				column.setMinWidth(width);
				column.setMaxWidth(width);
				column.setPreferredWidth(width);
			}

			try {
				Object[] row;
				EmployeeDAO eDAO = new EmployeeDAO();
				ResultSet result = eDAO.queryForTable();
				while (result.next()) {
					row = new Object[] { result.getString(2), result.getString(3) };
					dTableModel.addRow(row);
				}
			} catch (Exception ex) {
			}
	}
	
	private void updateTable()
	{
		int c = dTableModel.getRowCount();
			for (int i=c-1; i>=0; i--)
			{
				dTableModel.removeRow(i);
			}
			drawMyTable();
	}
	

	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame("Manager Window");
		frame.setBounds(100, 100, 759, 331);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		btnCreate = new JButton("Create employee account");
		btnCreate.setBounds(27, 32, 217, 23);
		panel.add(btnCreate);

		btnUpdate = new JButton("Update employee account");
		btnUpdate.setBounds(27, 176, 217, 23);
		panel.add(btnUpdate);

		btnDelete = new JButton("Delete employee account");
		btnDelete.setBounds(27, 220, 217, 23);
		panel.add(btnDelete);

		table.setFont(new Font("Serif", Font.PLAIN, 18));
		table.setRowHeight(table.getRowHeight() + 5);
		table.setAutoCreateRowSorter(true);
		table.setColumnSelectionAllowed(false);
		table.setRowSelectionAllowed(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(322, 11, 419, 279);
		panel.add(scrollPane);
		
		JLabel lblPleaseNoteThat = new JLabel("<html>Please note, that leaving any of the<br>fields empty will have no effect!</html>");
		lblPleaseNoteThat.setBounds(27, 113, 236, 38);
		panel.add(lblPleaseNoteThat);
		
		lblTheTwoButtons = new JLabel("the two buttons work on selected row");
		lblTheTwoButtons.setBounds(27, 162, 264, 14);
		panel.add(lblTheTwoButtons);

	
		drawMyTable();

btnCreate.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent e) {
		   JTextField userName = new JTextField();
		   JTextField userPassword = new JTextField();
		   Object[] message = { "Username: ", userName, "Password: ",userPassword };

		   Object[] options = { "Create!", "Cancel" };
		   int n = JOptionPane
				   .showOptionDialog(new JFrame(), message,
						   "Add Employee User", JOptionPane.YES_NO_OPTION,
						   JOptionPane.QUESTION_MESSAGE, null, options,options[1]);
		if (n == JOptionPane.OK_OPTION) { // Afirmative
			String newUsername = userName.getText();
			String newPassword = userPassword.getText();
			EmployeeDAO addToDb = new EmployeeDAO();
			Employee newEmployee = new Employee(newUsername,newPassword, 0);//add the new employee account
			if (newUsername.trim().length() > 0 &&
					newUsername.trim().length() > 0)//in case both fields are completed
			{
				if(addToDb.checkPreValidity(newUsername, newPassword))//check if username is not taken
				{
					addToDb.addEmployee(newEmployee);
					updateTable();
					JOptionPane.showMessageDialog(frame,"Sucessfully added desk employee " + newUsername+"!","Add Desk Employee!",
							JOptionPane.INFORMATION_MESSAGE);
				}
				else 
				{
					JOptionPane.showMessageDialog(frame,
						    "Sorry, username "+newUsername+" is already taken!",
						    "USERNAME TAKEN",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		if (n == JOptionPane.CLOSED_OPTION) { // closed the dialog
			// ....
		}

	}
});
		
btnDelete.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		
		
		if (table.getRowCount()>0)//checking if table is empty
		{
			if(table.getSelectedRow()!=-1)//check if there is a row selected for deletion
			{
				int n = JOptionPane.showConfirmDialog(
					    frame,
					    "Are you sure you want to remove user: "+(String)table.getValueAt(table.getSelectedRow(),0),
					    "Please confirm deletion!",
					    JOptionPane.YES_NO_OPTION);	
				if (n==JOptionPane.YES_OPTION)
				{
						EmployeeDAO eDAO = new EmployeeDAO();
						String a = (String)table.getValueAt(table.getSelectedRow(),0);
						String b = (String)table.getValueAt(table.getSelectedRow(),1);
						eDAO.removeEmployee(a,b);
						updateTable();
						JOptionPane.showMessageDialog(frame,"Sucessfully Deleted desk employee "+a+"!","Add Desk Employee!",
								JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}
	}
});
		
btnUpdate.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		if (table.getRowCount()>0)//checking if table is empty
		{
			if (table.getSelectedRow()!=-1)//check if row for update is selected
			{
				String a = (String)table.getValueAt(table.getSelectedRow(),0);//username from selected row
				String b = (String)table.getValueAt(table.getSelectedRow(),1);//password from selected row
				
				JTextField userName = new JTextField(a);
				JTextField userPassword = new JTextField(b);
				Object[] message = { "Username: ", userName, "Password: ",userPassword };

				Object[] options = { "Update!", "Cancel" };
				int n = JOptionPane.showOptionDialog(new JFrame(), message,"Update Employee User", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE, null, options,options[1]);
				if (n == JOptionPane.OK_OPTION) { // Afirmative
					EmployeeDAO eDAO = new EmployeeDAO();
			
					String newUsername = userName.getText();
					String newPassword = userPassword.getText();
		
					//check for empty fields and existing username
					if(eDAO.checkPreValidity(newUsername, newPassword))//if not a duplicate
					{
						if(newUsername.trim().length() > 0 && newPassword.trim().length() > 0)//if both fields are filled
						{
							eDAO.updateEmployee(a, b, newUsername, newPassword);
							updateTable();
							JOptionPane.showMessageDialog(frame,"Sucessfully updated desk employee "+newUsername+"!", 
									"Update Desk Employee!",JOptionPane.INFORMATION_MESSAGE);
						}
					}
					else{//if we already have this username
							//if prevalidity has failed, but selection we want to keep username and change password
							if (a.equals(newUsername))//if selected row coincides with inputed username
							{ //than we clearly wish to update the selected user's account
								eDAO.updateEmployee(a, b, newUsername, newPassword);
								updateTable();
								JOptionPane.showMessageDialog(frame,"Sucessfully updated desk employee "+newUsername+"!", "Update Desk Employee!",
											JOptionPane.INFORMATION_MESSAGE);
							}
							else{
								JOptionPane.showMessageDialog(frame,
									    "Sorry, can't update to username "+newUsername+". It is already taken!",
									    "USERNAME TAKEN",
									    JOptionPane.ERROR_MESSAGE);
							}
						}		
				}
				if (n == JOptionPane.CLOSED_OPTION) { // closed the dialog
				// ....
				}
				}
		}
	}
});

	}//end of init function
}//end of class
