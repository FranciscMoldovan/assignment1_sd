package DataAccessLayer;

public class Customer {


	private String name;
	private String identityCardNb;
	private String personalNumericalCode;
	private String address;
	private String phoneNb;
	
	
public Customer (String name, String identityCardNb, String personalNumericalCode, String address, 
				 String phoneNb){
	this.name=name;
	this.identityCardNb=identityCardNb;
	this.personalNumericalCode=personalNumericalCode;
	this.address=address;
	this.phoneNb=phoneNb;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getIdentityCardNb() {
	return identityCardNb;
}

public void setIdentityCardNb(String identityCardNb) {
	this.identityCardNb = identityCardNb;
}

public String getPersonalNumericalCode() {
	return personalNumericalCode;
}

public void setPersonalNumericalCode(String personalNumericalCode) {
	this.personalNumericalCode = personalNumericalCode;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getPhoneNb() {
	return phoneNb;
}

public void setPhoneNb(String phoneNb) {
	this.phoneNb = phoneNb;
}



@Override
public String toString() {
	return "Customer [name=" + name + ", identityCardNb=" + identityCardNb
			+ ", personalNumericalCode=" + personalNumericalCode
			+ ", address=" + address + ", phoneNb=" + phoneNb+"]";
}

}

