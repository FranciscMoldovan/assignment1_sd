package DataAccessLayer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import CommonUtilityLayer.MySQLConnect;

public class CustomerDAO {
	public CustomerDAO(){
		
	}
	
	private Connection myConn;
	private Statement myStat;
	private ResultSet result;
	
	public ArrayList<Customer> getAllCustomers() throws SQLException
	{
		ResultSet myRes=null;
		Customer cust = null;
		ArrayList<Customer> custList = new ArrayList<Customer>();
		try{
		 myConn = MySQLConnect.getConnection();
		 myStat = myConn.createStatement();	
		 myRes = myStat.executeQuery("select * from client");
		 while(myRes.next())
		 {
			 cust=new Customer(myRes.getString("name"),myRes.getString("id_card_nb"), myRes.getString("pers_num_code"),myRes.getString("address"),myRes.getString("phone"));
			 custList.add(cust);
		 }
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return custList;
		
	}
	
	public void addCustomer(Customer cust) 
	{
		try{
			
			 String name=cust.getName();
			 String identityCardNb=cust.getIdentityCardNb();
			 String personalNumericalCode=cust.getPersonalNumericalCode();
			 String address=cust.getAddress();
			 String phoneNb=cust.getPhoneNb();
		
		System.out.println("TO BE ADDED:");
		cust.toString();	 
			 
		 myConn = MySQLConnect.getConnection();
		 myStat = myConn.createStatement();	
		 
		 myStat.executeUpdate("INSERT INTO client(name,id_card_nb,pers_num_code,address,phone)" +
		 " VALUES ('"+name+"','"+identityCardNb+"', '"+ personalNumericalCode +"','"+address+"','"+phoneNb+"')" );
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

	public boolean checkPreValidity(String identityCardNb, String personalNumericalCode)
	{
		/**
		 * boolean variable to avoid adding a duplicate user
		 */
		boolean notRepeated=false;
		try{
			ResultSet myRes=null;
			Customer cust = null;
			 myConn = MySQLConnect.getConnection();
			 myStat = myConn.createStatement();	
			 myRes = myStat.executeQuery("SELECT * FROM client WHERE id_card_nb =\'"+identityCardNb+"\' or pers_num_code='"+personalNumericalCode+"'");
			
			if (myRes!=null)
			{
		    if(myRes.next())
		    {
			 while(myRes.next())
			 {
				 cust=new Customer(myRes.getString("name"),myRes.getString("id_card_nb"),myRes.getString("pers_num_code"), 
						 		   myRes.getString("address"),myRes.getString("phone"));
			 }
		    }else notRepeated=true;
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		System.out.println("CKECK PREVALIDITY ZICE:"+notRepeated);
	return notRepeated;

	}
	
	
	public void removeCustomer(String identityCardNb, String personalNumericalCode)
	{
		try{
			myConn = MySQLConnect.getConnection();
			myStat = myConn.createStatement();	
			myStat.executeUpdate("DELETE FROM client WHERE id_card_nb =\'"+identityCardNb+"\' and pers_num_code='"+personalNumericalCode+"'" );
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	public void updateCustomer(String identityCardNb, String personalNumericalCode, String a,String b,String c,String d,String e)
	{
		try{
			myConn = MySQLConnect.getConnection();
			myStat = myConn.createStatement();	
			myStat.executeUpdate("UPDATE client SET name='"+a+"',id_card_nb='"+b+"',pers_num_code='"+c+"',address='"+d+"',phone='"+e+"' WHERE id_card_nb =\'"+identityCardNb+"\' and pers_num_code='"+personalNumericalCode+"'");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/**
	 * DAO Method to query Database for retrieving the table bank clients to 
	 * the desk employee's window
	 * @return result, ResultSet
	 */
	public ResultSet queryForTable(){	
		ResultSet myRes=null;
		try{
			 myConn = MySQLConnect.getConnection();
			 myStat = myConn.createStatement();	
			 myRes = myStat.executeQuery("select * from client");
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			    
		System.out.println("QUERY FOR TABLE:");
		return myRes;
	}

}















