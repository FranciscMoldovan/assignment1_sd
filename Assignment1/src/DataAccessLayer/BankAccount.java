package DataAccessLayer;

import java.util.Date;

public class BankAccount {
	private String accountType;
	private double amount;
	private Date creationDate;
	private String identityNb;
	private int ownerID;
	
	
	public BankAccount(String type, double amount, Date created, 
			  		   String ident, int owner){
		this.accountType=type;
		this.amount=amount;
		this.creationDate=created;
		this.identityNb=ident;
		this.ownerID=owner;
	}


	public String getAccountType() {
		return accountType;
	}


	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public Date getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	public String getIdentityNb() {
		return identityNb;
	}


	public void setIdentityNb(String identityNb) {
		this.identityNb = identityNb;
	}


	public int getOwnerID() {
		return ownerID;
	}


	public void setOwnerID(int ownerID) {
		this.ownerID = ownerID;
	}


	@Override
	public String toString() {
		return "BankAccount [accountType=" + accountType + ", amount=" + amount
				+ ", creationDate=" + creationDate + ", identityNb="
				+ identityNb + ", ownerID=" + ownerID + "]";
	}
}
