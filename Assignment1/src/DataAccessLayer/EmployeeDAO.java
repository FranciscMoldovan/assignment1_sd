package DataAccessLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import CommonUtilityLayer.MySQLConnect;

public class EmployeeDAO {
	
	private Connection myConn;
	private Statement myStat;
	private ResultSet result;
	
	public EmployeeDAO() 
	{
	}
	

public ArrayList<Employee> getAllEmployees() throws SQLException
{
	ResultSet myRes=null;
	Employee user = null;
	ArrayList<Employee> userList = new ArrayList<Employee>();
	try{
	 myConn = MySQLConnect.getConnection();
	 myStat = myConn.createStatement();	
	 myRes = myStat.executeQuery("select * from employee");
	 while(myRes.next())
	 {
		 user=new Employee(myRes.getString("username"),myRes.getString("password"), myRes.getInt("role"));
		 userList.add(user);
	 }
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	
	return userList;
	
}

public void addEmployee(Employee employee) 
{
	try{
		String username=employee.getUsername();
		String password=employee.getPassword();
		int role=employee.getRole();
	 myConn = MySQLConnect.getConnection();
	 myStat = myConn.createStatement();	
	 myStat.executeUpdate("INSERT INTO employee(username,password,role) " + "VALUES ('"+username+"','"+password+"', "+ role +")");
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	
}

public boolean checkPreValidity(String username, String password)
{
	/**
	 * boolean variable to avoid adding a duplicate user
	 */
	boolean notRepeated=false;
	try{
		ResultSet myRes=null;
		Employee user = null;
		 myConn = MySQLConnect.getConnection();
		 myStat = myConn.createStatement();	
		 myRes = myStat.executeQuery("SELECT * FROM employee WHERE username =\'"+username+"\'");
		
		if (myRes!=null)
		{
	    if(myRes.next())
	    {
		 while(myRes.next())
		 {
			 user=new Employee(myRes.getString("username"),myRes.getString("password"), myRes.getInt("role"));
			 System.out.println(myRes.getString("username"));
			 System.out.println(myRes.getString("password"));
			 System.out.println(myRes.getInt("role"));
		 }
	    }else notRepeated=true;
		}
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	System.out.println(notRepeated);
return notRepeated;

}

public void removeEmployee(String username, String password)
{
	try{
		myConn = MySQLConnect.getConnection();
		myStat = myConn.createStatement();	
		myStat.executeUpdate("DELETE FROM employee WHERE username="+"\'"+username+"\'"+"and password="+"\'"+password+"\'" );
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
}


public void updateEmployee(String oldUsername, String oldPassword, String newUsername, String newPassword)
{
	try{
		myConn = MySQLConnect.getConnection();
		myStat = myConn.createStatement();	
		myStat.executeUpdate("UPDATE employee SET password='"+newPassword+"',username='"+newUsername+"' WHERE username='"+oldUsername+"' and password='"+oldPassword+"'");
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
}



public boolean logInEmployee(String username, String password){
	boolean okToLogIn=false;
	ResultSet myRes=null;
	Employee user = null;
	
	try{
		 myConn = MySQLConnect.getConnection();
		 myStat = myConn.createStatement();	
		 myRes = myStat.executeQuery("SELECT * FROM employee WHERE username =\""+username+"\" and password=\""+password+"\"");
		
		if (myRes!=null)
		{
		 while(myRes.next())
		 {
			 user=new Employee(myRes.getString("username"),myRes.getString("password"), myRes.getInt("role"));
			 System.out.println(myRes.getString("username"));
			 System.out.println(myRes.getString("password"));
			 System.out.println(myRes.getInt("role"));
			 okToLogIn=true;
		 }
		}
		}catch(Exception e)
		{
			
		}


	
	if (user==null)
	{
		okToLogIn=false;
	}	
		
	return okToLogIn;
}

/**
 * DAO Method to query Database for retrieving the table of front desk employees to 
 * the manager's window
 * @return result, ResultSet
 */
public ResultSet queryForTable(){	
	ResultSet myRes=null;
	try{
		 myConn = MySQLConnect.getConnection();
		 myStat = myConn.createStatement();	
		 myRes = myStat.executeQuery("select * from employee");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		    
	System.out.println("QUERY FOR TABLE:");
	return myRes;
}



}


















