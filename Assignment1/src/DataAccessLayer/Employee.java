package DataAccessLayer;

public class Employee {

	private String username; 
	private String password; 
	private int role; 
	
	
	
	public Employee(String username, String password, int role) {
		this.username=username;
		this.password=password;
		this.role=role;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}
	
	public int getRole(){
		return this.role;
	}
	
	public void setUsername(String username){
		this.username=username;
	}
	
	public void setPassword(String password){
		this.password=password;
	}

	@Override
	public String toString() {
		return "Employee [username=" + username + ", password=" + password
				+ ", role=" + role + "]";
	}

	

}
